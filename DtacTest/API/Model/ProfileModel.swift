//
//  ProfileModel.swift
//  DtacTest
//
//  Created by Too’s MacBook Pro on 24/3/2564 BE.
//

import Foundation

struct ProfileModel: Codable {
    let results: [Result]
    
    struct Result: Codable {
        let name: Name
        let email: String
        let dob: Dob
        let picture: Picture
    }
    
    struct Dob: Codable {
        let age: Int
    }
    
    struct Name: Codable {
        let title, first, last: String
    }
    
    struct Picture: Codable {
        let thumbnail: String
    }
}
