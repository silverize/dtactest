//
//  Resource.swift
//  DtacTest
//
//  Created by Too’s MacBook Pro on 24/3/2564 BE.
//

import Foundation

struct Resource {
    let url: URL
    let method: String = "GET"
}
