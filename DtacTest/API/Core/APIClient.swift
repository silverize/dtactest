//
//  APIClient.swift
//  DtacTest
//
//  Created by Too’s MacBook Pro on 24/3/2564 BE.
//

import Foundation

final class APIClient {
    
    func fetch(_ resource: Resource, result: @escaping ((Result<Data>) -> Void)) {
        let request = URLRequest(resource)
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                guard let data = data else {
                    result(.failure(APIClientError.noData))
                    return
                }
                print(data.prettyPrinted ?? "No JSON object")
                if let error = error {
                    result(.failure(error))
                    return
                }
                result(.success(data))
            }
        }
        task.resume()
    }
}
