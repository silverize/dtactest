//
//  UIViewControllerExtension.swift
//  DtacTest
//
//  Created by Too’s MacBook Pro on 24/3/2564 BE.
//

import UIKit

extension UIViewController {
    func startLoading() {
        let alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.medium
        loadingIndicator.startAnimating()
        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
    }
    
    func stopLoading(completion: @escaping () -> Void) {
        dismiss(animated: true, completion: completion)
    }
}
