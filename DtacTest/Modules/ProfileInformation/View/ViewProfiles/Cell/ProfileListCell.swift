//
//  ProfileListCell.swift
//  DtacTest
//
//  Created by Too’s MacBook Pro on 24/3/2564 BE.
//

import UIKit
import Kingfisher

class ProfileListCell: UITableViewCell {
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    
    struct Data {
        let profileImage: String
        let name: String
        let age: String
        let email: String
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setupData(data: Data) {
        profileImageView.kf.setImage(with: URL(string: data.profileImage))
        nameLabel.text = "Name : \(data.name)"
        ageLabel.text = "Age : \(data.age)"
        emailLabel.text = "Email : \(data.email)"
    }
}
