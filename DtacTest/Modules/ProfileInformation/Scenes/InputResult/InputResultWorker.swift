//
//  InputResultWorker.swift
//  DtacTest
//
//  Created by Too’s MacBook Pro on 24/3/2564 BE.
//

import Foundation

class InputResultWorker {
    
    private let apiClient: APIClient!
    
    init(apiClient: APIClient) {
        self.apiClient = apiClient
    }
    
    func fetchResult(request: InputResult.FetchResult.Request, _ completion: @escaping ((Result<ProfileModel>) -> Void)) {
        let resource = Resource(url: URL(string: "https://randomuser.me/api/?results=\(request.qty)")!)
        apiClient.fetch(resource) { (result) in
            switch result {
            case .success(let data):
                do {
                    let items = try JSONDecoder().decode(ProfileModel.self, from: data)
                    completion(.success(items))
                } catch {
                    completion(.failure(error))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
