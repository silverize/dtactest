//
//  ViewProfilesInteractor.swift
//  DtacTest
//
//  Created by Too’s MacBook Pro on 24/3/2564 BE.
//  Copyright (c) 2564 BE ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol ViewProfilesBusinessLogic {
    func getProfileList(request: ViewProfiles.GetProfileList.Request)
}

protocol ViewProfilesDataStore {
    var profileList: [ProfileModel.Result]? { get set }
}

class ViewProfilesInteractor: ViewProfilesBusinessLogic, ViewProfilesDataStore {
    var presenter: ViewProfilesPresentationLogic?
    var worker: ViewProfilesWorker?
    var profileList: [ProfileModel.Result]?
    
    func getProfileList(request: ViewProfiles.GetProfileList.Request) {
        let response = ViewProfiles.GetProfileList.Response(profileList: profileList)
        presenter?.presentGetProfileList(response: response)
    }
}
